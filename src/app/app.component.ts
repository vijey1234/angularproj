import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  title = 'Angularone';
  
  constructor(private _activatedRoute: ActivatedRoute, private _Router: Router) {}
  
  gotoFirstComponent() {
      this._Router.navigateByUrl('/first');
      // this._Router.navigate(['./first']);

  }

  // goBack() {
  //   this._Router.navigate(['./'], {relativeTo: path})
  // }

  gotoSecondComponent() {
    this._Router.navigateByUrl('/second');

  }
}
