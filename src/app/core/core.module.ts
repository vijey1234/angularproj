import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';




@NgModule({
  declarations: [LoginComponent, ForgetPasswordComponent, ],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
