import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondfComponent } from './secondf.component';

describe('SecondfComponent', () => {
  let component: SecondfComponent;
  let fixture: ComponentFixture<SecondfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SecondfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
